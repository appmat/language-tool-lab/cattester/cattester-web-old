import requests

from flask import current_app

ROOT = current_app.config['SERVER_API_ROOT']


class ApiRoutes:
    FILES = 'files'
    PROBLEMS = 'problems'
    SUBMISSIONS = 'submissions'
    THEMES = 'themes'
    USERS = 'users'


class ApiHelper:
    @staticmethod
    def _make_request(func, route, args, kwargs):
        tail = "/".join(str(arg) for arg in args)
        response = func(f'{ROOT}/{route}/{tail}', params=kwargs)
        if response.ok and response.text:
            return response.json()

    @classmethod
    def get(cls, route, *args, **kwargs):
        return cls._make_request(requests.get, route, args, kwargs)

    @classmethod
    def post(cls, route, *args, **kwargs):
        return cls._make_request(requests.post, route, args, kwargs)
