from dataclasses import dataclass

from dataclasses_json import dataclass_json


@dataclass_json
@dataclass
class ProblemDTO:
    id: int
    theme_id: int
    title: str
    description: str
    last_update_time: str


@dataclass_json
@dataclass
class ThemeDTO:
    id: int
    name: str


@dataclass_json
@dataclass
class UserDTO:
    id: int
    email: str
    name: str
    surname: str
    patronymic: str
    pass_hash: str


@dataclass_json
@dataclass
class SubmissionDTO:
    id: int
    problem_id: int
    status: str


@dataclass_json
@dataclass
class FilesDTO:
    name: str
    value: str
