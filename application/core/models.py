from dataclasses import dataclass, field
from typing import List


@dataclass
class Problem:
    id: int
    theme: str
    theme_id: int
    title: str
    description: str
    last_update_time: str


@dataclass
class ProblemListElement:
    id: int
    title: str


@dataclass
class Theme:
    id: int
    title: str


@dataclass
class File:
    name: str
    value: str


@dataclass
class Submission:
    id: int
    problem_id: int
    status: str
    files: List[File] = field(default_factory=list)
