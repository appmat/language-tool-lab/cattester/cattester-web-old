from application.core.api_helper import ApiHelper, ApiRoutes
from application.core.dtos import ProblemDTO, ThemeDTO, SubmissionDTO, FilesDTO
from application.core.models import Problem, ProblemListElement, Theme, Submission


class ProblemService:
    @staticmethod
    def _problem_from_dto(problem_dto: ProblemDTO):
        response_theme = ApiHelper.get(ApiRoutes.THEMES, problem_dto.theme_id)
        theme = ThemeDTO.from_dict(response_theme)
        return Problem(
            problem_dto.id,
            theme.name,
            theme.id,
            problem_dto.title,
            problem_dto.description,
            problem_dto.last_update_time
        )

    @staticmethod
    def _problem_list_element_from_dto(problem_list_element_dto: ProblemDTO):
        return ProblemListElement(
            problem_list_element_dto.id,
            problem_list_element_dto.title
        )

    @classmethod
    def get_all(cls):
        response = ApiHelper.get(ApiRoutes.PROBLEMS)
        dto_list = ProblemDTO.schema().load(response, many=True)
        return [cls._problem_list_element_from_dto(dto) for dto in dto_list]

    @classmethod
    def get_by_id(cls, problem_id):
        response = ApiHelper.get(ApiRoutes.PROBLEMS, problem_id)
        dto = ProblemDTO.from_dict(response)
        return cls._problem_from_dto(dto)

    @classmethod
    def get_by_theme_id(cls, theme_id):
        response = ApiHelper.get(ApiRoutes.THEMES, theme_id, ApiRoutes.PROBLEMS)
        dto_list = ProblemDTO.schema().load(response, many=True)
        return [cls._problem_list_element_from_dto(dto) for dto in dto_list]


class ThemeService:
    @staticmethod
    def _theme_from_dto(theme_dto: ThemeDTO):
        response_theme = ApiHelper.get(ApiRoutes.THEMES, theme_dto.id)
        theme = ThemeDTO.from_dict(response_theme)
        return Theme(
            theme_dto.id,
            theme.name
        )

    @classmethod
    def get_all(cls):
        response = ApiHelper.get(ApiRoutes.THEMES)
        dto_list = ThemeDTO.schema().load(response, many=True)
        return [cls._theme_from_dto(dto) for dto in dto_list]

    @classmethod
    def get_by_id(cls, theme_id):
        response = ApiHelper.get(ApiRoutes.THEMES, theme_id)
        dto = ThemeDTO.from_dict(response)
        return cls._theme_from_dto(dto)


class SubmissionService:
    @staticmethod
    def _submission_from_dto(submission_dto: SubmissionDTO):
        response_files = ApiHelper.get(ApiRoutes.PROBLEMS, submission_dto.problem_id, ApiRoutes.SUBMISSIONS,
                                       submission_dto.id, ApiRoutes.FILES)
        files = FilesDTO.schema().load(response_files, many=True)
        return Submission(
            submission_dto.id,
            submission_dto.problem_id,
            submission_dto.status,
            files
        )

    @classmethod
    def get_all(cls, problem_id):
        response = ApiHelper.get(ApiRoutes.PROBLEMS, problem_id, ApiRoutes.SUBMISSIONS)
        dto_list = SubmissionDTO.schema().load(response, many=True)
        return [cls._submission_from_dto(dto) for dto in dto_list]
