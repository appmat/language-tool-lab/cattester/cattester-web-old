from flask import render_template, send_from_directory, abort, request, flash, redirect, url_for

from application.core.services import ProblemService, ThemeService, SubmissionService
from application.web import web


@web.route('/favicon.ico')
def favicon():
    return send_from_directory("static", 'favicon.ico', mimetype='image/vnd.microsoft.icon')


@web.route('/', methods=['GET'])
@web.route('/index', methods=['GET'])
def index():
    return redirect(url_for('web.themes'))


@web.route('/themes', methods=['GET'])
def themes():
    themes = ThemeService.get_all()
    if not themes:
        abort(404)
    return render_template('themes.html', themes=themes)


@web.route('/themes/<theme_id>/problems', methods=['GET'])
def problems(theme_id):
    theme = ThemeService.get_by_id(theme_id)
    problems = ProblemService.get_by_theme_id(theme_id)
    if not problems:
        abort(404)
    return render_template('problems.html', problems=problems, theme=theme)


@web.route('/problems/<problem_id>', methods=['GET', 'POST'])
def problem(problem_id):
    problem = ProblemService.get_by_id(problem_id)
    submissions = SubmissionService.get_all(problem_id)
    if not problem:
        abort(404)
    if request.method == 'POST':
        flash('Решение успешно отправлено', category='success')
    return render_template('problem.html', problem=problem, submissions=submissions)


@web.route('/login', methods=['GET', 'POST'])
def login():
    return render_template('login.html')


@web.route('/signup', methods=['GET', 'POST'])
def signup():
    return render_template('signup.html')


@web.errorhandler(404)
def error_404(error_info):
    return render_template('error_page.html', title='404', error_info=error_info, error_code='404',
                           error_text='страница не найдена'), 404


@web.errorhandler(500)
@web.errorhandler(Exception)
def error_500(error_info):
    return render_template('error_page.html', title='500', error_info=error_info, error_code='500',
                           error_text='внутренняя ошибка сервера'), 500
