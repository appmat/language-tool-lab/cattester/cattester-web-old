from flask import Flask


def create_app():
    """Create Flask application."""
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_json('config.json')

    with app.app_context():
        # Import application parts
        from application.web import web as web_bp

        # Register Blueprints
        app.register_blueprint(web_bp)

        return app
